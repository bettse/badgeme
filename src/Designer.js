import React, {useState, useEffect} from 'react';
import {Prompt} from 'react-router-dom';
import AceEditor from 'react-ace';

import {When} from 'react-if';

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Form from 'react-bootstrap/Form';
import Alert from 'react-bootstrap/Alert';
import Button from 'react-bootstrap/Button';

import 'ace-builds/src-noconflict/mode-json';
import 'ace-builds/src-noconflict/theme-github';

import RenderTemplate from './RenderTemplate';

import Template from './Template';

function Designer(props) {
  const [crd, setCrd] = useState(null);
  const [background, setBackground] = useState(null);
  const [template, setTemplate] = useState(Template.example);
  const [textTemplate, setTextTemplate] = useState(
    JSON.stringify(Template.example, null, 4),
  );
  const [status, setStatus] = useState({variant: '', message: ''});
  const [dirty, setDirty] = useState(false);

  useEffect(() => {
    const extractPng = async () => {
      try {
        const t = new Template(crd);
        await t.parse();

        setBackground(t.dataUrl);
        setTextTemplate(t.text);
      } catch (e) {
        console.log('error extracting crd:', e);
        setCrd(null);
      }
    };

    if (crd) {
      if (crd.type === 'image/png') {
        extractPng();
      } else {
        console.log('unhandled type', crd.type);
        setCrd(null);
      }
    }
  }, [crd]);

  async function download() {
    const t = new Template(crd);
    try {
      await t.parse();
      await t.updateTemplate(template);
      t.download();
      setDirty(false);
    } catch (e) {
      console.log('error with download', e);
      setStatus({variant: 'warning', message: 'error with download'});
    }
  }

  useEffect(() => {
    if (textTemplate === JSON.stringify(template, null, 4)) {
      // Skip if it is the same (usually on load)
      return;
    }
    try {
      const t = JSON.parse(textTemplate);
      setTemplate(t);
      setStatus({variant: '', message: ''});
    } catch (e) {
      setStatus({variant: 'danger', message: 'invalid json'});
    }
  }, [textTemplate, template]);

  return (
    <>
      <Prompt
        when={dirty}
        message={location =>
          `Are you sure you want to go to ${location.pathname}`
        }
      />
      <Row>
        <Col>
          <Form.Group>
            <Form.Text>
              Templates are a PNG with embedded JSON.  Check out some examples in the gallery or upload a regular png to add JSON to it.
            </Form.Text>
            <Form.File
              custom
              onChange={e => setCrd(e.target.files[0])}
              label="Card template"
            />
          </Form.Group>
        </Col>
      </Row>
      <Row>
        <Col>
          <AceEditor
            mode="json"
            theme="github"
            name="ace-editor"
            readOnly={!crd}
            value={textTemplate}
            onChange={update => {
              setTextTemplate(update);
              setDirty(true);
            }}
            editorProps={{
              $blockScrolling: true,
            }}
            setOptions={{
              useWorker: false,
            }}
          />
        </Col>
        <Col>
          <When condition={status.variant !== ''}>
            <Alert variant={status.variant}>{status.message}</Alert>
          </When>
          <div>
            <div style={{display: 'inline-block', border: '1px dashed black'}}>
              <RenderTemplate
                orientation={template.orientation}
                background={background}
                fonts={template.fonts}
                overlays={template.overlays}
              />
            </div>
          </div>
        </Col>
      </Row>
      <Row className="mt-3 text-center justify-content-center">
        <Col>
          <Button
            variant={dirty ? 'info' : 'primary'}
            disabled={!crd}
            onClick={download}>
            Download
          </Button>
          <Form.Text>Saves as PNG with embedded template</Form.Text>
        </Col>
      </Row>
    </>
  );
}

export default Designer;
