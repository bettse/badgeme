import React, { useState, useEffect, useContext, useRef } from "react";
import { Link, useParams, useHistory } from "react-router-dom";

import { If, Then, Else, When } from "react-if";
import Dropzone from "react-dropzone";

import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Alert from "react-bootstrap/Alert";
import Spinner from "react-bootstrap/Spinner";
import Image from "react-bootstrap/Image";

import pngSize from "png-size";

import { Context } from "./Provider";
import Shipping from "./Shipping";
import PrinterStatus from "./PrinterStatus";
import PlaceholderBadge from "./PlaceholderBadge";

const { localStorage } = window;

const { NODE_ENV } = process.env;

const wsUrl = "wss://websocket.ericbetts.dev";
const now = new Date();

//TODO: handle re-connect
const ws = new WebSocket(wsUrl);
ws.onopen = event => {
  setInterval(() => {
    ws.send(
      JSON.stringify({
        action: "echo",
        value: new Date().getTime()
      })
    );
  }, 5 * 60 * 1000);
};

// Portrait
const badgePx = {
  height: 1012,
  width: 638
};

function fitsBadge({ width, height }) {
  const portrait = badgePx.width === width && badgePx.height === height;
  const landscape = badgePx.width === height && badgePx.height === width;
  const halfPortrait =
    badgePx.width / 2 === width && badgePx.height / 2 === height;
  const halfLandscape =
    badgePx.width / 2 === height && badgePx.height / 2 === width;

  return portrait || landscape || halfPortrait || halfLandscape;
}

function BadgeRequest() {
  const history = useHistory();
  const { requestId } = useParams();
  const [updates, setUpdates] = useState([]);
  const { badge, setBadge } = useContext(Context);
  const imagePreviewEl = useRef(null);
  const [showSizeWarning, setShowSizeWarning] = useState(false);
  const [submitting, setSubmitting] = useState(false);
  const maintenance =
    (now.getUTCDay() === 6 && now.getUTCHours() >= 18) ||
    (now.getUTCDay() === 0 && now.getUTCHours() < 6);

  useEffect(() => {
    async function getLaststatus() {
      const response = await fetch(
        `/.netlify/functions/request?id=${requestId}`
      );

      if (response.ok) {
        const { status, statusVariant } = await response.json();
        setUpdates([{ status, statusVariant }, ...updates]);
      }
    }

    if (requestId) {
      setSubmitting(false);
      ws.onmessage = message => {
        const { data } = message;
        try {
          const parsed = JSON.parse(data);
          const { state } = parsed;
          //Ignore subscribed message
          if (state && state === "subscribed") {
            return;
          }
          const { status = "", statusVariant = "info" } = parsed;
          if (status !== "") {
            setUpdates([{ status, statusVariant }, ...updates]);
          }
        } catch (e) {
          console.log(e);
        }
      };

      ws.onopen = event => {
        ws.send(
          JSON.stringify({
            action: "subscribe",
            // TODO: try this as `key`
            path: `badgesOf/request/${requestId}`
          })
        );
      };

      if (updates.length === 0) {
        getLaststatus();
      }
    }
  }, [requestId, updates]);

  useEffect(() => {
    const { current } = imagePreviewEl;
    if (current && badge) {
      current.src = URL.createObjectURL(badge);
    }
  }, [imagePreviewEl, badge]);

  useEffect(() => {
    const extractPng = async () => {
      try {
        const buffer = new Uint8Array(await badge.arrayBuffer());
        const dimensions = pngSize(buffer);
        setShowSizeWarning(!fitsBadge(dimensions));
      } catch (e) {
        console.log("error with image:", e);
      }
    };

    if (badge) {
      if (badge.type === "image/png") {
        extractPng();
      }
    }
  }, [badge]);

  const handleSubmit = async event => {
    setSubmitting(true);
    event.preventDefault();
    const { target } = event;
    const formData = new FormData(target);
    formData.set("badge", badge, badge.name);
    setUpdates([
      { status: "form submitted", statusVariant: "info" },
      ...updates
    ]);

    try {
      const response = await fetch("/.netlify/functions/request", {
        method: "POST",
        body: formData
      });
      const { status, statusVariant, id } = await response.json();
      setUpdates([{ status, statusVariant }, ...updates]);

      if (response.ok) {
        const object = Object.fromEntries(formData);
        localStorage.setItem("form", JSON.stringify(object));
        history.push(`/request/${id}`);
      }
    } catch (e) {
      console.log(e);
      setUpdates([
        { status: "form submission failed", statusVariant: "warning" },
        ...updates
      ]);
      setSubmitting(false);
    }
  };

  const previousJSON = localStorage.getItem("form");
  let previous = {};
  if (previousJSON) {
    previous = JSON.parse(previousJSON);
  }

  return (
    <>
      <When condition={maintenance}>
        <Alert variant="warning">
          Maintenance window, submitted jobs will be queued
        </Alert>
      </When>
      <When condition={submitting}>
        <Alert variant="info" className="text-center">
          <Spinner animation="grow" role="status" />
          Submitting...
          <Spinner animation="grow" role="status" />
        </Alert>
      </When>
      <If condition={!requestId}>
        <Then>
          <Form onSubmit={handleSubmit}>
            <Row>
              <Col>
                <Card>
                  <Card.Header>Badge</Card.Header>
                  <Card.Body>
                    <Dropzone
                      onDrop={acceptedFiles => setBadge(acceptedFiles[0])}
                    >
                      {({ getRootProps, getInputProps }) => (
                        <div {...getRootProps()}>
                          <input {...getInputProps()} />
                          <If condition={badge === null}>
                            <Then>
                              <PlaceholderBadge />
                            </Then>
                            <Else>
                              <Image
                                fluid
                                ref={imagePreviewEl}
                                alt="preview of customized template or uploaded image file"
                              />
                            </Else>
                          </If>
                        </div>
                      )}
                    </Dropzone>

                    <div className="spacer mt-3" />
                    <When condition={showSizeWarning}>
                      <Alert variant="warning">
                        Selected image doesn&#39;t match badge aspect ratio
                      </Alert>
                    </When>
                    <When condition={NODE_ENV === 'development'}>
                      <Form.Group as={Row} controlId="formHorizontalUrl">
                        <Form.Label column sm={2}>
                          URL to encode
                        </Form.Label>
                        <Col sm={10}>
                          <Form.Control
                            required
                            name="url"
                            type="url"
                            defaultValue={previous.url}
                          />
                        </Col>
                      </Form.Group>
                    </When>
                    <Form.Group as={Row} controlId="formHorizontalEmail">
                      <Form.Label column sm={2}>
                        Email
                      </Form.Label>
                      <Col sm={6}>
                        <Form.Control
                          required
                          name="email"
                          type="email"
                          defaultValue={previous.email}
                        />
                      </Col>
                      <Col sm={4}>
                        <Form.Text id="emailHelpBlock">
                          Validated using{" "}
                          <a href="https://emailrep.io/">
                            https://emailrep.io/
                          </a>
                        </Form.Text>
                      </Col>
                    </Form.Group>
                    <Form.Group as={Row} controlId="formGridNotes">
                      <Form.Label column sm={2}>
                        Notes
                      </Form.Label>
                      <Col sm={6}>
                        <Form.Control
                          as="textarea"
                          autoCorrect="on"
                          rows="3"
                          name="notes"
                          defaultValue={previous.notes}
                        />
                      </Col>
                      <Col sm={4}>
                        <Form.Text id="notesHelpBlock">
                          Anything else: words of appreciation, details of
                          pre-arranged requests, feature suggestions, etc.
                        </Form.Text>
                      </Col>
                    </Form.Group>
                    <Form.Group as={Row} controlId="formHorizontalGallery">
                      <Col sm={6}>
                        <Form.Switch
                          name="gallery"
                          label={
                            <span>
                              Include image in{" "}
                              <Link to="/gallery">gallery</Link>
                            </span>
                          }
                        />
                      </Col>
                    </Form.Group>
                  </Card.Body>
                </Card>
              </Col>
              <Col md={12} lg={6}>
                <PrinterStatus detail={false}/>
                <Card>
                  <Card.Header>
                    Destination{previousJSON ? " (Loaded from cache)" : ""}
                  </Card.Header>
                  <Card.Body>
                    <Form.Group as={Row} controlId="formHorizontalName">
                      <Form.Label required column sm={2}>
                        Name
                      </Form.Label>
                      <Col sm={6}>
                        <Form.Control
                          name="name"
                          type="text"
                          placeholder="Name for address label"
                          defaultValue={previous.name}
                        />
                      </Col>
                    </Form.Group>

                    <Form.Group controlId="formGridAddress1">
                      <Form.Label>Address</Form.Label>
                      <Form.Control
                        required
                        type="text"
                        name="address1"
                        placeholder="1234 Main St"
                        defaultValue={previous.address1}
                      />
                    </Form.Group>

                    <Form.Group controlId="formGridAddress2">
                      <Form.Label>Address 2</Form.Label>
                      <Form.Control
                        name="address2"
                        type="text"
                        placeholder="Apartment, studio, or floor"
                        defaultValue={previous.address2}
                      />
                    </Form.Group>

                    <Form.Row>
                      <Form.Group as={Col} md="4" controlId="formGridCity">
                        <Form.Label>City</Form.Label>
                        <Form.Control
                          required
                          type="text"
                          name="city"
                          defaultValue={previous.city}
                        />
                      </Form.Group>

                      <Form.Group as={Col} md="5" controlId="formGridRegion">
                        <Form.Label>State / Province / Region</Form.Label>
                        <Form.Control
                          type="text"
                          name="region"
                          defaultValue={previous.region}
                        />
                      </Form.Group>

                      <Form.Group as={Col} md="3" controlId="formGridPostal">
                        <Form.Label>Postal Code</Form.Label>
                        <Form.Control
                          required
                          type="text"
                          name="postalcode"
                          defaultValue={previous.postalcode}
                        />
                      </Form.Group>
                    </Form.Row>

                    <Form.Group as={Row} controlId="formGridCountry">
                      <Form.Label column sm={3}>
                        Country
                      </Form.Label>
                      <Col sm={5}>
                        <Form.Control
                          type="text"
                          name="country"
                          defaultValue={previous.country || "USA"}
                        />
                      </Col>
                    </Form.Group>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
            <Row className="pt-5">
              <Col className="text-center justify-content-center">
                <Button variant="success" disabled={submitting} type="submit">
                  Request
                </Button>
              </Col>
            </Row>
          </Form>

          <Row className="pt-5">
            <Col xs={12}>
              <Shipping />
              <PrinterStatus />
            </Col>
          </Row>
        </Then>
        <Else>
          <Row>
            <Col md={12} lg={6}>
              <Shipping />
            </Col>
            <Col md={12} lg={6}>
              <Card>
                <Card.Header>Updates</Card.Header>
                <Card.Body>
                  {updates.map((update, i) => {
                    const { status, statusVariant } = update;
                    return (
                      <Alert
                        key={i}
                        className="text-capitalize"
                        variant={statusVariant}
                      >
                        {status}
                      </Alert>
                    );
                  })}
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Else>
      </If>
    </>
  );
}

export default BadgeRequest;
