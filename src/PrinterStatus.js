import React, { useEffect } from "react";

import { When } from "react-if";
import { useLocalStorage } from 'react-use';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import Badge from 'react-bootstrap/Badge';
import ProgressBar from 'react-bootstrap/ProgressBar';

const card_types = [
  'PVC', 'NTAG213', 'NTAG215', 'NTAG216', 't55xx', 'NTAG424', 'iClass'
].sort();
const expiry = 60 * 1000;
const now = Date.now();

function PrinterStatus(props) {
  const { detail } = props;
  const [printerStatus, setPrinterStatus] = useLocalStorage('printerStatus', {});
  const [lastLoad, setLastLoad] = useLocalStorage('printerStatus-lastLoad', 0);
  const { tag = {}, status = {}, stock = {} } = printerStatus;
  const { label, qty, count } = tag;
  const { cleaning, feeder, hopper, ribbon_finished } = status;
  const { card_type, global_stamps, us_stamps, envelopes, last_updated } = stock;

  useEffect(() => {
    async function getPrinterStatus() {
      try {
        const response = await fetch('/.netlify/functions/status');
        if (response.ok) {
          const {status} = await response.json();
          setPrinterStatus(status);
          setLastLoad(now);
        }
      } catch(e) {
        console.log(e);
      }
    }
    if (now - lastLoad > expiry) {
      getPrinterStatus();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const notices = [];
  if (cleaning && cleaning !== 'OK') {
    notices.push('Cleaning cycle needed');
  }
  if (feeder && feeder !== 'CARD') {
    notices.push('Feeder empty');
  }
  if (ribbon_finished && ribbon_finished !== 'OK') {
    notices.push('Ribbon exhausted');
  }
  if (hopper && hopper === 'CARD') {
    notices.push('Printed card in hopper');
  }

  function badges() {
    return card_types.map((card, i) => {
      const current = card_type && card_type.toLowerCase() === card.toLowerCase()
      const variant = current ? 'success' : 'secondary'
      return (
        <span key={i}>
        {` `}<Badge key={i} variant={variant}>{card}</Badge>
        </span>
      );
    });
  }

  if (detail === false) {
    return (
      <>
        <Card>
          <Card.Header>Printer Status</Card.Header>
          <Card.Body>
            <div>
              <span>Card Type:</span>
              {badges()}
            </div>
            <div>
              <span>Ribbon: {label}</span>
              <ProgressBar style={{height: '24px', fontSize: '20px'}} striped variant="info" min={0} max={qty} now={count || 0} label={`${count}/${qty}`} />
            </div>
          </Card.Body>
        </Card>
      </>
    );
  }

  return (
    <>
      <Card>
        <Card.Header>Printer Details</Card.Header>
        <Card.Body>
          <Row>
            <Col xs={4}>
              <Card.Title>Current Ribbon</Card.Title>
              <Card.Text>{count}/{qty} prints remain for <code>{label}</code> ribbon.</Card.Text>
            </Col>

            <Col xs={4}>
              <Card.Title>Current Stock</Card.Title>
              <ListGroup>
                <ListGroup.Item className="text-capitalize">Card type loaded: {card_type}</ListGroup.Item>
                <ListGroup.Item className="text-capitalize">Global stamps: {global_stamps}</ListGroup.Item>
                <ListGroup.Item className="text-capitalize">US stamps: {us_stamps}</ListGroup.Item>
                <ListGroup.Item className="text-capitalize">Envelopes: {envelopes}</ListGroup.Item>
                <ListGroup.Item className="text-capitalize">Last hand count: {(new Date(last_updated)).toLocaleDateString()}</ListGroup.Item>
              </ListGroup>
            </Col>

            <Col xs={4}>
              <Card.Title>Current Notices</Card.Title>
              <When condition={notices.length === 0}>
                <Card.Text>None</Card.Text>
              </When>
              <When condition={notices.length > 0}>
                <ListGroup>
                  {notices.map(notice => <ListGroup.Item>{notice}</ListGroup.Item>)}
                </ListGroup>
              </When>
            </Col>
          </Row>
        </Card.Body>
      </Card>
    </>
  );
}

export default PrinterStatus;
