import pngChunksExtract from "png-chunks-extract";
import pngChunkText from "png-chunk-text";
import pngChunksEncode from "png-chunks-encode";
import { saveAs } from "file-saver";

const TemplateKeyword = "crd:template";

class Template {
  constructor(png) {
    this.png = png;
  }

  async parse() {
    const ab = await this.png.arrayBuffer()
    const buffer = new Uint8Array(ab);
    const chunks = pngChunksExtract(buffer);
    const template = chunks
      .filter(chunk => chunk.name === "tEXt")
      .map(chunk => pngChunkText.decode(chunk.data))
      .find(({ keyword }) => keyword === "crd:template");

    if (template) {
      const { text } = template;
      this.text = text;

      //TODO: support other keys in the json, maybe a background image color?
      const {
        overlays = [],
        orientation = "portrait",
        author = {},
        fonts = []
      } = JSON.parse(text);
      this.overlays = overlays;
      this.orientation = orientation;
      this.author = author;
      this.fonts = fonts;
    }

    // I wish I could do this:
    //setBackground(URL.createObjectURL(newBackground, {type: 'image/png'}));
    // But like when using a URL, the image isn't kept in the downloaded svg-to-png
    const reader = new FileReader();
    reader.readAsDataURL(this.png);
    this.dataUrl = await new Promise((resolve, reject) => {
      reader.addEventListener(
        "load",
        () => {
          resolve(reader.result);
        },
        false
      );
    });
  }

  async updateTemplate(template) {
    try {
      const content = JSON.stringify(template, null, 4);
      const ab = await this.png.arrayBuffer();
      const buffer = new Uint8Array(ab);

      // Remove existing template
      const chunks = pngChunksExtract(buffer).filter(chunk => {
        if (chunk.name === "tEXt") {
          const decoded = pngChunkText.decode(chunk.data);
          if (decoded.keyword === TemplateKeyword) {
            return false;
          }
        }
        return true;
      });

      const chunk = pngChunkText.encode(TemplateKeyword, content);
      // Add new/updated template
      chunks.splice(-1, 0, chunk);

      const uint8Array = pngChunksEncode(chunks);
      this.png = new Blob([uint8Array], { type: "image/png" });
    } catch (e) {
      console.log(e);
    }
  }

  download() {
    saveAs(this.png, this.png.name || "new.png");
  }
}

Template.example = {
  crdVersion: 1,
  author: {
    name: "bettse",
    gitlab: "bettse",
    github: "bettse",
    twitter: "@aguynamedbettse",
    email: "bettse@fastmail.fm",
    anything: "value"
  },
  orientation: "portrait",
  fonts: ["https://fonts.googleapis.com/css2?family=Odibee+Sans&display=swap"],
  overlays: [
    {
      type: "text",
      label: "custom font",
      x: 319,
      y: 90,
      "text-anchor": "middle",
      style: {
        "font-size": "40px",
        "font-family": "Odibee Sans, Arial"
      }
    },
    {
      type: "image",
      label: "example image",
      x: 159,
      y: 260,
      width: 320,
      height: 240
    },
    {
      type: "qrcode",
      label: "qrcode",
      x: 300,
      y: 650,
      width: 240,
      height: 240
    },
    {
      type: "text",
      label: "text centered",
      x: 319,
      y: 572,
      "text-anchor": "middle",
      style: {
        "text-transform": "capitalize",
        "font-size": "72px"
      }
    },
    {
      type: "text",
      label: "text left",
      x: 10,
      y: 818,
      style: {
        "text-transform": "uppercase",
        "font-size": "24px"
      }
    },
    {
      type: "barcode",
      label: "barcode",
      format: "code128",
      x: 10,
      y: 930,
      width: 618,
      height: 72
    }
  ]
};

export default Template;
