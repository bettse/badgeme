import React, {useState, useEffect, useContext, useRef} from 'react';
import {useParams, useHistory} from 'react-router-dom';

import {If, Then, Else, When} from 'react-if';

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
import ProgressBar from 'react-bootstrap/ProgressBar';
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';

import toImg from './svg-to-image';
import {Context} from './Provider';
import RenderTemplate from './RenderTemplate';
import TemplateGallery from './TemplateGallery';
import TemplateUpload from './TemplateUpload';
import OverlayInput from './OverlayInput';

import Template from './Template';

function TemplateCustomizer(props) {
  const { templateId } = useParams();
  const history = useHistory();
  const {setBadge} = useContext(Context);

  const [crd, setCrd] = useState(null);
  const [background, setBackground] = useState(null);
  const [overlays, setOverlays] = useState([]);
  const [fonts, setFonts] = useState([]);
  const [orientation, setOrientation] = useState('portrait');
  const [author, setAuthor] = useState({});
  const [show, setShow] = useState(false);
  const [download, setDownload] = useState(false);
  const [template, setTemplate] = useState(null);

  const modalBody = useRef(null);
  const svg = useRef(null);

  useEffect(() => {
    if (templateId) {
      console.log(templateId)
    }
  }, [templateId])

  useEffect(() => {
    if (template) {
      return fetch(template)
      .then(data => data.blob())
      .then(crd => setCrd(crd));
    }
  }, [template])

  useEffect(() => {
    const extractPng = async () => {
      try {
        const t = new Template(crd);
        await t.parse();
        setBackground(t.dataUrl)
        setOverlays(t.overlays);
        setOrientation(t.orientation);
        setAuthor(t.author);
        setFonts(t.fonts);
      } catch (e) {
        console.log('error extracting crd:', e);
        setCrd(null);
      }
    };

    if (crd) {
      if (crd.type === 'image/png') {
        extractPng();
      } else {
        console.log('unhandled type', crd.type);
        setCrd(null);
      }
    }
  }, [crd]);

  async function fileToDataUrl(file) {
    const reader = new FileReader();
    reader.readAsDataURL(file);

    return new Promise((resolve, reject) => {
      reader.addEventListener('load', () => {
        resolve(reader.result);
      }, false);
    });
  }

  function handleChange(index) {
    // Return function to handle onChange event
    return async event => {
      const {target} = event;
      const overlay = overlays[index];
      const {type} = overlay;
      const newOverlays = [...overlays];

      switch (type) {
        case 'barcode':
        case 'qrcode':
        case 'text':
          const {value} = target;
          newOverlays[index] = {
            ...overlay,
            value,
          };
          setOverlays(newOverlays);
          break;
        case 'image':
          const {files} = target;
          const [file] = files;
          const dataUrl = await fileToDataUrl(file);

          newOverlays[index] = {
            ...overlay,
            value: dataUrl,
          };
          setOverlays(newOverlays);
          break;
        default:
          console.log('no handler for', type);
      }
    };
  }


  async function svgClick(event) {
    const {shiftKey} = event;
    setDownload(shiftKey);
    setShow(true);
  }

  // In order to prevent the svg-to-image's clone of the svg from
  // creating a flash on screen, we put it in a modal
  useEffect(() => {
    const processImg = async () => {
      try {
        var done = false;
        setTimeout(() => {
          // Close the modal if the process has comleted within a second
          if (done) {
            if (download) {
              setShow(false);
            } else {
              history.push('/request');
            }
          }
        }, 1000);
        if (download) {
          await toImg(svg.current, 'card', {modal: modalBody.current});
          done = true;
        } else {
          const data = await toImg(svg.current, 'card', {
            download,
            modal: modalBody.current,
          })
            .then(imageDataUrl => fetch(imageDataUrl))
            .then(data => data.blob());
          setBadge(data);
          done = true;
        }
      } catch (e) {
        console.log('error in processImg', e);
      }
    };
    if (show && modalBody.current && svg.current) {
      processImg();
    }
  }, [show, modalBody, svg, download, setBadge, history]);

  return (
    <>
      <If condition={crd === null}>
        <Then>
          <TemplateGallery setTemplate={setTemplate} />
          <Row>
            <Col>
              <Form.Group>
                <Form.Text>
                  Or upload a template from your computer
                </Form.Text>
                <Form.File
                  custom
                  onChange={e => setCrd(e.target.files[0])}
                  label="Card template"
                />
              </Form.Group>
            </Col>
          </Row>
        </Then>
        <Else>
          <Row>
            <Col>
              <When condition={author.name !== undefined}>
                <Card className="mb-3">
                  <Card.Header>Designed by {author.name}</Card.Header>
                  <ListGroup variant="flush">
                    {Object.entries(author)
                      .filter(kv => kv[0] !== 'name')
                      .map((kv, i) => {
                        return (
                          <ListGroup.Item key={i}>
                            {kv[0]}: {kv[1]}
                          </ListGroup.Item>
                        );
                      })}
                  </ListGroup>
                </Card>
              </When>
              {overlays.map((overlay, i) => {
                return <OverlayInput key={i} overlay={overlay} onChange={handleChange(i)}/>
              })}
            </Col>
            <Col>
              <div>
                <div style={{display: 'inline-block', border: '1px dotted grey'}}>
                  <RenderTemplate
                    scale={0.5}
                    ref={svg}
                    onClick={svgClick}
                    orientation={orientation}
                    background={background}
                    overlays={overlays}
                    fonts={fonts}
                  />
                </div>
              </div>
              <p>
                Click the image to load into badge request form (hold shift to download)
              </p>
            </Col>
          </Row>
        </Else>
      </If>

      <TemplateUpload />

      <Modal show={show} onHide={() => setShow(false)} centered size="xl">
        <Modal.Header>
          <Modal.Title>Rendering image</Modal.Title>
        </Modal.Header>
        <Modal.Body
          ref={modalBody}
          className="text-center justify-content-center">
          <ProgressBar animated now={100} />
        </Modal.Body>
      </Modal>
    </>
  );
}

export default TemplateCustomizer;
