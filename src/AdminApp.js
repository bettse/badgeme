import React, { useEffect } from "react";

import { When } from "react-if";
import { useLocalStorage } from 'react-use';

import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import CardColumns from "react-bootstrap/CardColumns";
import Button from "react-bootstrap/Button";
import Image from "react-bootstrap/Image";
import Alert from "react-bootstrap/Alert";
import Accordion from 'react-bootstrap/Accordion';
import Form from 'react-bootstrap/Form';

function byId(a, b) {
  return -1 * a._id.localeCompare(b._id);
}

function AdminApp() {
  const [requests, setRequests] = useLocalStorage('admin-requests', []);

  function run(id, card = true, letter = true) {
    return async event => {
      const response = await fetch("/.netlify/functions/admin-requests", {
        method: "POST",
        body: JSON.stringify({ id, card, letter })
      });
      console.log("POSTed to sockethook", response.ok);
    };
  }

  useEffect(() => {
    async function getRequests() {
      try {
        const response = await fetch("/.netlify/functions/admin-requests");
        if (response.ok) {
          const json = await response.json();
          const { requests } = json;
          requests.sort(byId)
          setRequests(requests)
        }
      } catch (e) {
        console.log(e);
      }
    }
    getRequests();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleSubmit = async event => {
    event.preventDefault();
    const {target} = event;
    const formData = new FormData(target);
    const body = JSON.stringify(Object.fromEntries(formData));
    try {
      await fetch('/.netlify/functions/status', {
        method: 'POST',
        body,
      });
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <>
      <Navbar collapseOnSelect expand="sm" bg="danger" variant="danger">
        <Navbar.Brand>Admin: Badges of</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto"></Nav>
          <Nav>
            <Navbar.Text>
              <img
                src="https://api.netlify.com/api/v1/badges/2866ca28-537d-41a7-991b-6a246e7fff5c/deploy-status"
                alt="Netlify Status"
              />
            </Navbar.Text>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      <Container fluid className="mt-3">
        <Row>
          <Col>

            <Accordion>
              <Card>
                <Accordion.Toggle as={Card.Header} eventKey="0">
                  Update stock
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="0">
                  <Card.Body>
                    <Form inline onSubmit={handleSubmit}>

                      <Form.Group controlId="formHorizontalCardType">
                        <Form.Label column sm={6}>
                          Card stock loaded
                        </Form.Label>
                        <Form.Control name="card_type" type="text" />
                      </Form.Group>

                      <Form.Group controlId="formHorizontalGlobalStamps">
                        <Form.Label column sm={6}>
                          Global Stamps
                        </Form.Label>
                        <Form.Control name="global_stamps" type="number" />
                      </Form.Group>

                      <Form.Group controlId="formHorizontalUsStamps">
                        <Form.Label column sm={6}>
                          US Stamps
                        </Form.Label>
                        <Form.Control name="us_stamps" type="number" />
                      </Form.Group>

                      <Form.Group controlId="formHorizontalEnvelopes">
                        <Form.Label column sm={6}>
                          Envelopes
                        </Form.Label>
                        <Form.Control name="envelopes" type="number" />
                      </Form.Group>


                      <Button type="submit">Submit</Button>
                    </Form>
                  </Card.Body>
                </Accordion.Collapse>
              </Card>
            </Accordion>


          </Col>
        </Row>
        <Row>
          <Col>
            <CardColumns>
              {requests.map((request, i) => {
                const {
                  address1,
                  address2,
                  city,
                  country,
                  region,
                  postalcode
                } = request;
                const { notes, _id: id, name, email, badge } = request;
                const { uid, status } = request;
                return (
                  <Card key={i}>
                    <Card.Header>{id}</Card.Header>
                    <Card.Body>
                      <Card.Title>{name}</Card.Title>
                      <When condition={status && status.length > 0}>
                        <Alert variant='info'>{status}</Alert>
                      </When>
                      <Row>
                        <Col>
                          <address>
                            {address1}
                            <br />
                            <When condition={address2 && address2.length > 0}>
                              {address2}
                              <br />
                            </When>
                            {city}, {region} &nbsp;{postalcode}
                            <br />
                            {country}
                            <br />
                          </address>
                          <Card.Text>
                            <When condition={uid && uid.length > 0}>
                              UID: {uid}<br/>
                            </When>
                            {email}
                            <br />
                            <When condition={notes && notes.length > 0}>
                              Notes: {notes}
                            </When>
                          </Card.Text>
                        </Col>
                        <Col>
                          <Image
                            fluid
                            style={{
                              border: "1px dotted grey",
                              borderRadius: "10px"
                            }}
                            src={badge.url}
                          />
                        </Col>
                      </Row>

                      <Button variant="info" onClick={run(id)}>
                        Run
                      </Button>
                      <Button variant="secondary" onClick={run(id, true, false)}>
                        Card
                      </Button>
                      <Button variant="secondary" onClick={run(id, false, true)}>
                        Letter
                      </Button>

                    </Card.Body>
                  </Card>
                );
              })}
            </CardColumns>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default AdminApp;
