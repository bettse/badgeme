import React from 'react';
import { useParams } from "react-router-dom";

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Card from 'react-bootstrap/Card';

function NTAG21x() {
  const { uid } = useParams();

  /*
  * TODO:
  * * get request object from backend
  * * Show badge, last status?, probably not address
  * * Update record with status 'used'/'arrived'/'delivered'/'confirmed'
  */

  return (
    <>
      <Row>
        <Col>
          <Card>
            <Card.Header>{uid}</Card.Header>
            <Card.Body>
              <Card.Text>
                Stuff goes here.
              </Card.Text>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </>
  );
}

export default NTAG21x;
