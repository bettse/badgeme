import React from 'react';

import {When} from 'react-if';

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Form from 'react-bootstrap/Form';

function OverlayInput(props) {
  const { overlay, onChange } = props;
  const {type, label, comment} = overlay;
  switch (type) {
    case 'qrcode':
    case 'barcode':
    case 'text':
      return (
        <>
          <When condition={comment && comment.length > 0}>
            <Form.Text>{comment}</Form.Text>
          </When>
          <Form.Group as={Row} controlId={`formHorizontal${overlay.label}`}>
            <Form.Label column xs="auto">
              {label}
            </Form.Label>
            <Col xs="auto">
              <Form.Control type="text" onChange={onChange} />
            </Col>
          </Form.Group>
        </>
      );
    case 'image':
      return (
        <Form.Group>
          <When condition={comment && comment.length > 0}>
            <Form.Text>{comment}</Form.Text>
          </When>
          <Form.File
            custom
            onChange={onChange}
            label={`${overlay.width}x${overlay.height} ${label}`}
          />
        </Form.Group>
      );
    default:
      return null;
  }
}


export default OverlayInput;
