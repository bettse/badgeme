import React from 'react';

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Card from 'react-bootstrap/Card';
import CardDeck from 'react-bootstrap/CardDeck';
import Button from 'react-bootstrap/Button';

import cardImage from'./card.png';
import designImage from'./design.png';
import templateImage from'./template.png';

const style = {
	maxWidth: '319px'
};

function Home() {
  return (
    <>
      <Row>
        <Col>
          <h4 className="pb-3">Choose your adventure:</h4>
          <CardDeck className="justify-content-center">
            <Card style={style} key="request">
              <Card.Img variant="top" src={cardImage} href="/request" />
              <Card.Body>
                <Card.Title className="text-center">Request</Card.Title>
                <Card.Text>
                  Upload your custom designed image, provide your address, and a badge will be printed and shipped to you.
                </Card.Text>
                <Button variant="primary" href="/request">Request</Button>
              </Card.Body>
            </Card>

            <Card style={style} key="templates">
              <Card.Img variant="top" src={templateImage} />
              <Card.Body>
                <Card.Title className="text-center">Fill in</Card.Title>
                <Card.Text>
                  Start with an existing template: Chose from a user contribute teplate, fill in your details, and request a badge.
                </Card.Text>
                <Button variant="primary" href="/templates">Fill In</Button>
              </Card.Body>
            </Card>

            <Card style={style} key="design">
              <Card.Img variant="top" src={designImage} />
              <Card.Body>
                <Card.Title className="text-center">Design</Card.Title>
                <Card.Text>
                  Upload a PNG, and define text, images, and barcodes to create a template of your own.
                </Card.Text>
                <Button variant="primary" href="/design">Design</Button>
              </Card.Body>
            </Card>

          </CardDeck>
        </Col>
      </Row>
    </>
  );
}

export default Home;
