import React from 'react';

import Alert from 'react-bootstrap/Alert';

function Maintenance() {
  return (
    <>
      <Alert variant="warning" className="text-center">
        <h2>Offline for maintenance</h2>
        <h4>(Every Saturday 6pm to Sunday 6am UTC)</h4>
      </Alert>
    </>
  );
}

export default Maintenance;
