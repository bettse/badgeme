import React, {useState, useEffect, forwardRef} from 'react';
import QRCode from 'qrcode.react';

import Barcode39 from './Barcode39';
import Barcode128 from './Barcode128';

function camelCaseKeys(obj) {
  return Object.fromEntries(
    Object.entries(obj).map(([key, val]) => {
      const camelCased = key.replace(/-([a-z])/g, g => g[1].toUpperCase());
      return [camelCased, val];
    }),
  );
}

function placeholderImage(overlay) {
  const {width, height, label} = overlay;
  const text = `${width}x${height} ${label}`;
  return `https://dummyimage.com/${width}x${height}/eee/aaa&text=${text}`;
}

function RenderTemplate(props, ref) {
  const {
    scale = 0.5,
    onClick = () => {},
    orientation = 'portrait',
    fonts = [],
    background,
    overlays = [],
  } = props;
  const [badgePx, setBadgePx] = useState({height: 0, width: 0});

  useEffect(() => {
    switch (orientation) {
      case 'landscape':
        setBadgePx({
          width: 1012,
          height: 638,
        });
        break;
      case 'portrait':
        setBadgePx({
          width: 638,
          height: 1012,
        });
        break;
      default:
        console.log('unknown orientation', orientation);
    }
  }, [orientation]);

  function renderOverlay(overlay) {
    const {
      type,
      x,
      y,
      r = 0,
      width,
      height,
      label,
      value,
      preserveAspectRatio = 'xMidYMid slice',
      style,
    } = overlay;
    switch (type) {
      case 'text':
        const {applyWidthAfterCharLength} = overlay;
        const textAnchor = overlay['text-anchor'];
        const content = value || label;
        const textLength =
          content.length > applyWidthAfterCharLength ? width : undefined;
        return (
          <g transform={`rotate (${r} ${x} ${y})`}>
            <text
              x={x}
              y={y}
              textAnchor={textAnchor}
              textLength={textLength}
              lengthAdjust="spacingAndGlyphs"
              style={camelCaseKeys(style)}>
              {value || label}
            </text>
          </g>
        );
      case 'barcode':
        if (overlay.format === 'code128') {
          return (
            <Barcode128
              x={x}
              y={y}
              width={width}
              height={height}
              value={value || label}
            />
          );
        } else {
          return (
            <Barcode39
              x={x}
              y={y}
              width={width}
              height={height}
              value={value || label}
            />
          );
        }
      case 'image':
        return (
          <g transform={`translate(${x} ${y})`}>
            <image
              preserveAspectRatio={preserveAspectRatio}
              mask={`url(#mask${width}x${height}x${overlay.r})`}
              href={value || placeholderImage(overlay)}
              width={width}
              height={height}
            />
          </g>
        );
      case 'qrcode':
        const {fgColor, bgColor} = overlay;
        return (
          <g transform={`translate(${x} ${y})`}>
            <QRCode
              renderAs="svg"
              size={Math.max(height, width)}
              fgColor={fgColor}
              bgColor={bgColor}
              value={value || window.location.toString()}
            />
          </g>
        );
      default:
        return null;
    }
  }

  function renderMasks() {
    // TODO: uniq masks based on width/height/r
    // TODO: create some sort of function for generating the mask id
    const masks = overlays
      .filter(overlay => overlay.type === 'image' && overlay.r > 0)
      .map(overlay => {
        const {height, width, r} = overlay;
        const key = `mask${width}x${height}x${r}`;
        return (
          <mask key={key} id={key}>
            <rect fill="white" rx={r} ry={r} width={width} height={height} />
          </mask>
        );
      });
    return <defs>{masks}</defs>;
  }

  function renderStyle() {
    if (fonts.length === 0) {
      return null;
    }
    return <style>{fonts.map(font => `@import url(${font});`)}</style>;
  }

  return (
    <svg
      onClick={onClick}
      ref={ref}
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      viewBox={`0 0 ${badgePx.width} ${badgePx.height}`}
      width={badgePx.width * scale}
      height={badgePx.height * scale}>
      {renderMasks()}
      {renderStyle()}
      <rect
        id="backgroundColor"
        rx="15"
        ry="15"
        fill="white"
        width={badgePx.width}
        height={badgePx.height}
      />
      <image
        id="backgroundImage"
        x="0"
        y="0"
        href={background}
        preserveAspectRatio="xMidYMid"
        height={badgePx.height}
        width={badgePx.width}
      />

      {overlays.map((overlay, i) => {
        return <g key={i}>{renderOverlay(overlay)}</g>;
      })}
    </svg>
  );
}

export default forwardRef(RenderTemplate);
