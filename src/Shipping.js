import React from 'react';

import Card from 'react-bootstrap/Card';

function Shipping(props) {
  return (
    <>
      <Card>
        <Card.Header>Shipping</Card.Header>
        <Card.Body>
          <ul>
            <li>
              Shipped USPS (contact me if you need an alternative carrier)
            </li>
            <li>
              Unfortunately there won't be any tracking, but if you don't
              receive your badge in 2 weeks (4 weeks international), feel free
              to request it again.
            </li>
          </ul>
        </Card.Body>
      </Card>
    </>
  );
}

export default Shipping;
