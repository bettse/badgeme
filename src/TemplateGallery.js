import React, {useState, useEffect} from 'react';

import {If, Then, Else, When} from 'react-if';
import { useLocalStorage } from 'react-use';

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Card from 'react-bootstrap/Card';
import CardColumns from 'react-bootstrap/CardColumns';
import Alert from 'react-bootstrap/Alert';
import Spinner from 'react-bootstrap/Spinner';

import Template from './Template';

const expiry = 60 * 1000;
const now = Date.now();

function TemplateGallery(props) {
  const { setTemplate } = props;
  const [status, setStatus] = useState('');
  const [statusVariant, setStatusVariant] = useState('info');
  const [templates, setTemplates] = useLocalStorage('templates', []);
  const [lastLoad, setLastLoad] = useLocalStorage('templates-lastLoad', 0);

  useEffect(() => {
    if (templates.length === 0) {
      setStatus('Loading...');
    }
    async function loadTemplates() {
      try {
        const resp = await fetch('/.netlify/functions/templates');
        if (resp.ok) {
          const json = await resp.json();
          const {templates} = json;
          const enrichedTemplates = templates.map(async template => {
            const { url } = template;
            const blob = await fetch(url).then(res => res.blob())
            const t = new Template(blob);
            await t.parse();
            return {
              author: t.author,
              overlays: t.overlays,
              ...template,
            }
          });
          Promise.all(enrichedTemplates).then(templates => {
            setTemplates(templates);
            setStatus('');
            if (templates.length > 0) {
              setLastLoad(now);
            }
          });
        } else {
          setStatusVariant('danger');
          setStatus('OFFLINE');
        }
      } catch (e) {
        setStatusVariant('danger');
        setStatus('OFFLINE');
      }
    }
    if (now - lastLoad > expiry) {
      loadTemplates();
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  function overlayDescription(overlays) {
    const counts = {};
    overlays.forEach(overlay => {
      const { type } = overlay;
      counts[type] = (counts[type] || 0) + 1;
    })
    const lis = Object.keys(counts).map((type, i) => {
      return (
        <li key={i}>{counts[type]} {type}</li>
      );
    })
    return (
      <ul>{lis}</ul>
    );
  }

  return (
    <>
      <Row>
        <Col>
          <When condition={status !== ''}>
            <Alert variant={statusVariant} className="text-center">
              <When condition={status === 'Loading...'}>
                <Spinner animation="grow" role="status" />
              </When>
              {status}
              <When condition={status === 'Loading...'}>
                <Spinner animation="grow" role="status" />
              </When>
            </Alert>
          </When>
        </Col>
      </Row>
      <Row>
        <Col>
          <h3>Templates</h3>
          <p>
          Click image to load into the template viewer and customize.  Right click to download.
          </p>
          <If condition={templates.length === 0}>
            <Then>
              <Alert variant="secondary" className="text-center">
                No templates to display. Why not submit one?
              </Alert>
            </Then>
            <Else>
              <CardColumns>
                {templates.map((template, i) => {
                  const {name, url, overlays = []} = template;
                  return (
                    <Card key={i} border="secondary" onClick={() => setTemplate(url)}>
                      <Card.Header className="text-center">{name}</Card.Header>
                      <Card.Body className="row align-items-center">
                        <Card.Img style={{border: '1px dotted grey', borderRadius: '10px'}} src={url} />
                        <When condition={overlays.length > 0}>
                          <p>Overlays:</p>
                          {overlayDescription(overlays)}
                        </When>
                      </Card.Body>
                    </Card>
                  );
                })}
              </CardColumns>
            </Else>
          </If>
        </Col>
      </Row>
    </>
  );
}

export default TemplateGallery;
