import React, {useState, useEffect} from 'react';
import { worldMill } from '@react-jvectormap/world';
import {VectorMap} from '@react-jvectormap/core';

import {When} from 'react-if';

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Alert from 'react-bootstrap/Alert';
import Spinner from 'react-bootstrap/Spinner';

function Map() {
  const [status, setStatus] = useState('Loading...');
  const [statusVariant, setStatusVariant] = useState('info');
  const [mapData, setMapData] = useState(null);

  useEffect(() => {
    async function loadLocations() {
      try {
        const resp = await fetch('/.netlify/functions/locations');
        if (resp.ok) {
          const json = await resp.json();
          const {countries} = json;
          countries['94c'] = countries['US'];
          setMapData(countries);
          setStatus('');
        } else {
          setStatusVariant('danger');
          setStatus('OFFLINE');
        }
      } catch (e) {
        console.log(e);
        setStatusVariant('danger');
        setStatus('OFFLINE');
      }
    }
    loadLocations();
  }, []);

  return (
    <>
      <Row>
        <Col>
          <When condition={status !== ''}>
            <Alert variant={statusVariant} className="text-center">
              <When condition={status === 'Loading...'}>
                <Spinner animation="grow" role="status" />
              </When>
              {status}
              <When condition={status === 'Loading...'}>
                <Spinner animation="grow" role="status" />
              </When>
            </Alert>
          </When>
        </Col>
      </Row>
      <Row>
        <Col>I have sent badges to recipients in these coutries</Col>
      </Row>
      <Row>
        <Col>
          <VectorMap
            map={worldMill}
            backgroundColor="transparent"
            zoomOnScroll={false}
            style={{
              width: '100%',
              height: '520px',
            }}
            regionStyle={{
              initial: {
                fill: '#e4e4e4',
                'fill-opacity': 0.9,
                stroke: 'none',
                'stroke-width': 0,
                'stroke-opacity': 0,
              },
              hover: {
                'fill-opacity': 0.8,
                cursor: 'pointer',
              },
            }}
            regionsSelectable={false}
            series={{
              regions: [
                {
                  values: mapData,
                  scale: ['#146804', '#ff0000'],
                  normalizeFunction: 'linear',
                },
              ],
            }}
          />
        </Col>
      </Row>
    </>
  );
}

export default Map;
