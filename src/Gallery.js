import React, {useState, useEffect} from 'react';

import {If, Then, Else, When} from 'react-if';
import { useLocalStorage } from 'react-use';

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Card from 'react-bootstrap/Card';
import CardColumns from 'react-bootstrap/CardColumns';
import Alert from 'react-bootstrap/Alert';
import Image from 'react-bootstrap/Image';
import Modal from 'react-bootstrap/Modal';
import Spinner from 'react-bootstrap/Spinner';

const expiry = 60 * 1000;
const now = Date.now();

function Gallery() {
  const [status, setStatus] = useState('');
  const [statusVariant, setStatusVariant] = useState('info');
  const [badges, setBadges] = useLocalStorage('badges', []);
  const [lastLoad, setLastLoad] = useLocalStorage('badges-lastLoad', 0);
  const [selected, setSelected] = useState(null);

  useEffect(() => {
    if (badges.length === 0) {
      setStatus('Loading...');
    }
    async function loadBadges() {
      try {
        const resp = await fetch('/.netlify/functions/gallery');
        if (resp.ok) {
          const json = await resp.json();
          const {badges} = json;
          setBadges(badges);
          setStatus('');
          if (badges.length > 0) {
            setLastLoad(now);
          }
        } else {
          setStatusVariant('danger');
          setStatus('OFFLINE');
        }
      } catch (e) {
        setStatusVariant('danger');
        setStatus('OFFLINE');
      }
    }
    if (now - lastLoad > expiry) {
      loadBadges();
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleClose = () => setSelected(null);
  const selectedBadge = badges[selected] || {};

  return (
    <>
      <Row>
        <Col>
          <When condition={status !== ''}>
            <Alert variant={statusVariant} className="text-center">
              <When condition={status === 'Loading...'}>
                <Spinner animation="grow" role="status" />
              </When>
              {status}
              <When condition={status === 'Loading...'}>
                <Spinner animation="grow" role="status" />
              </When>
            </Alert>
          </When>
        </Col>
      </Row>
      <Row>
        <Col>
          <h3>Badges</h3>
          <p>
            These are badges that have been printed, the submitter chose to
            include them in the gallery.
          </p>

          <If condition={badges.length === 0}>
            <Then>
              <Alert variant="secondary" className="text-center">
                No badges to display. Why not submit one?
              </Alert>
            </Then>
            <Else>
              <CardColumns>
                {badges.map((badge, i) => {
                  const {filename, url} = badge;
                  return (
                    <Card key={i} border="secondary" className="text-center">
                      <Card.Body>
                        <Card.Title>{filename}</Card.Title>
                        <Card.Img
                          src={url}
                          onClick={() => setSelected(i)}
                          style={{width: '50%'}}
                        />
                      </Card.Body>
                    </Card>
                  );
                })}
              </CardColumns>
            </Else>
          </If>
        </Col>
      </Row>
      <Modal centered size="lg" show={selected !== null} onHide={handleClose}>
        <Modal.Header closeButton>{selectedBadge.filename}</Modal.Header>
        <Modal.Body>
          <Image fluid src={selectedBadge.url} />
        </Modal.Body>
        <Modal.Footer>
          {selectedBadge.type} ({selectedBadge.size})
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default Gallery;
