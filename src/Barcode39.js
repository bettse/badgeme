import React, {useState, useEffect} from 'react';
import {Code39} from 'tualo-code39';

const code39 = new Code39();

function Barcode39(props) {
  const {x, y, width, height, value} = props;
  const [encoded, setEncoded] = useState([]);
  const [valid, setValid] = useState(true);
  /*
   * TODO: more options:
   *  * with/without text
   *  * text below vs text above
   *  * spread text
   *  * justify text: center/left/right?
   */

  useEffect(() => {
    try {
      const encoded = code39.getCode(value).split('');
      setEncoded(encoded);
      setValid(true);
    } catch (e) {
      setValid(false);
    }
  }, [value]);
  const barheight = height / 2;
  const countN = encoded.filter(x => x.toLowerCase() === 'n').length;
  const countW = encoded.length - countN;
  // "n" and "N" are 1/3 the width of w/W
  const visualLength = countN * 1 + countW * 3;
  const nb = Math.max(width / visualLength, 1);

  function generateBarcode() {
    let x = 0;

    return encoded.map((char, i) => {
      const width = char.toLowerCase() === 'w' ? 3 * nb : nb;
      const isUpperCase = char === char.toUpperCase();
      const fill = isUpperCase ? 'black' : 'white';
      x += width;
      return (
        <rect
          key={i}
          x={x - width}
          y="0"
          width={width}
          height={barheight}
          fill={fill}
        />
      );
    });
  }

  if (!valid) {
    return (
      <text
        textAnchor="middle"
        style={{fontSize: height, textTransform: 'uppercase'}}
        fill="red"
        x={x + width / 2}
        y={y + height}>
        Invalid barcode
      </text>
    );
  }

  return (
    <>
      <g transform={`translate(${x} ${y})`}>{generateBarcode()}</g>
      <text
        x={x + width / 2}
        y={y + height}
        textAnchor="middle"
        //textLength={width}
        style={{fontSize: height / 2}}>
        {value}
      </text>
    </>
  );
}

export default Barcode39;
