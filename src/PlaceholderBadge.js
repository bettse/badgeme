import React from "react";

import Card from "react-bootstrap/Card";

function PlaceholderBadge() {
  return (
    <>
      <div className="d-flex justify-content-center">
        <div
          style={{
            width: "319px",
            height: "506px",
            border: "1px dotted grey",
            borderRadius: "10px"
          }}
          className="d-flex align-items-center"
        >
          <div>
            <Card.Text className="text-center">
              Drag 'n' drop an image (png/jpeg) here, or click to select
            </Card.Text>
            <Card.Text className="ml-3">• 1MB limit</Card.Text>
            <Card.Text className="ml-3">• png/jpg only</Card.Text>
            <Card.Text className="ml-3">• Aim for 1012px ✕ 638px</Card.Text>
          </div>
        </div>
      </div>
    </>
  );
}

export default PlaceholderBadge;
