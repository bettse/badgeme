const inlineStyles = root => {
  const selfCopyCss = elt => {
    const computed = window.getComputedStyle(elt);
    const css = {};
    for (let i = 0; i < computed.length; i++) {
      css[computed[i]] = computed.getPropertyValue(computed[i]);
    }

    for (const key in css) {
      elt.style[key] = css[key];
    }
    return css;
  };

  selfCopyCss(root);
  root.querySelectorAll('*').forEach(elt => selfCopyCss(elt));
}; // inline styles

const copyToCanvas = ({elt: svg, scale, format, quality}) => {
  var svgData = new XMLSerializer().serializeToString(svg);
  var canvas = document.createElement('canvas');
  var svgSize = svg.getBoundingClientRect();

  //Resize can break shadows
  canvas.width = svgSize.width * scale;
  canvas.height = svgSize.height * scale;
  canvas.style.width = svgSize.width;
  canvas.style.height = svgSize.height;

  var ctxt = canvas.getContext('2d');
  ctxt.scale(scale, scale);

  var img = document.createElement('img');
  const data = btoa(unescape(encodeURIComponent(svgData)));
  img.setAttribute('src', 'data:image/svg+xml;base64,' + data);
  return new Promise(resolve => {
    img.onload = () => {
      ctxt.drawImage(img, 0, 0);
      const file = canvas.toDataURL(
        `image/${format}`,
        (format = 'png'),
        quality,
      );
      resolve(file);
    };
  });
};

const downloadImage = ({file, name, format}) => {
  var a = document.querySelector(`a#${name}-${format}`);

  if (!a) {
    a = document.createElement('a');
    a.id = `${name}-${format}`;
    a.className = 'display-none';
    a.download = `${name}.${format}`;
    document.body.appendChild(a);
  }
  a.href = file;
  a.click();
};

export default async function toImg(
  target,
  name,
  {
    scale = 1,
    format = 'png',
    quality = 0.92,
    download = true,
    modal = null,
  } = {},
) {
  const original = target;
  const elt = original.cloneNode(true);
  const {viewBox} = elt;
  const {baseVal} = viewBox;
  elt.setAttribute('height', baseVal.height);
  elt.setAttribute('width', baseVal.width);

  if (modal) {
    modal.appendChild(elt);
  } else {
    original.parentNode.appendChild(elt);
  }

  //Set all the css styles inline
  inlineStyles(elt);

  //Copy all html to a new canvas
  return await copyToCanvas({
    elt,
    scale,
    format,
    quality,
  })
    .then(file => {
      //Download if necessary
      if (download) downloadImage({file, name, format});

      if (!modal) {
        original.parentNode.removeChild(elt);
      }
      return file;
    })
    .catch(console.error);
}
