import React, {useState} from 'react';

export const Context = React.createContext();

const Provider = props => {
  const [badge, setBadge] = useState(null);

  return (
    <Context.Provider value={{badge, setBadge}}>
      {props.children}
    </Context.Provider>
  );
};

export default Provider;
