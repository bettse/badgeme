import React, {useState} from 'react';

import {When} from 'react-if';

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Card from 'react-bootstrap/Card';
import Alert from 'react-bootstrap/Alert';
import Spinner from 'react-bootstrap/Spinner';
import Accordion from 'react-bootstrap/Accordion';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

function TemplateUpload() {
  const [status, setStatus] = useState('');
  const [statusVariant, setStatusVariant] = useState('info');

  const handleSubmit = async event => {
    event.preventDefault();
    setStatus('Submitting...');
    const {target} = event;
    const formData = new FormData(target);

    try {
      const response = await fetch('/.netlify/functions/templates', {
        method: 'POST',
        body: formData,
      });
      if (response.ok) {
        const object = {};
        formData.forEach((value, key) => (object[key] = value));
        setTimeout(() => {
          setStatusVariant('');
          setStatus('');
        }, 3000);
        setStatusVariant('success');
        setStatus('Form Submission Successful!!');
      } else {
        setStatusVariant('warning');
        setStatus('Form Submission Failed!');
      }
    } catch (e) {
      console.log(e);
      setStatusVariant('warning');
      setStatus('Form Submission Failed!');
    }
  };

  return (
    <>
      <Row className="pt-5">
        <Col>
          <When condition={status !== ''}>
            <Alert variant={statusVariant} className="text-center">
              <When condition={status === 'Loading...'}>
                <Spinner animation="grow" role="status" />
              </When>
              {status}
              <When condition={status === 'Loading...'}>
                <Spinner animation="grow" role="status" />
              </When>
            </Alert>
          </When>
        </Col>
      </Row>
      <Row>
        <Col className="mt-5">
          <Accordion defaultActiveKey="0">
            <Card>
              <Accordion.Toggle as={Card.Header} eventKey="0">
                Upload new template to the gallery
              </Accordion.Toggle>
              <Accordion.Collapse eventKey="0">
                <Card.Body>
                  <Form onSubmit={handleSubmit}>
                    <Form.Group as={Row} controlId="formHorizontalName">
                      <Form.Label required column sm={2}>
                        Name
                      </Form.Label>
                      <Col sm={6}>
                        <Form.Control name="name" type="text" />
                      </Col>
                    </Form.Group>
                    <Form.Group as={Row} controlId="formHorizontalSecret">
                      <Form.Label required column sm={2}>
                        Secret
                      </Form.Label>
                      <Col sm={6}>
                        <Form.Control name="secret" type="text" />
                      </Col>
                      <Form.Text id="secretHelpBlock">
                        Secret will be used later for updating/deleting
                      </Form.Text>
                    </Form.Group>
                    <Form.Group as={Row}>
                      <Col md={12} lg={6}>
                        <Form.File
                          custom
                          required
                          name="template"
                          label="Template (png with embedded json)"
                        />
                      </Col>
                      <Col
                        md={12}
                        lg={6}
                        className="text-center justify-content-center">
                        <Button type="submit" disabled={status !== ''}>
                          Submit
                        </Button>
                      </Col>
                    </Form.Group>
                  </Form>
                </Card.Body>
              </Accordion.Collapse>
            </Card>
          </Accordion>
        </Col>
      </Row>
    </>
  );
}

export default TemplateUpload;
