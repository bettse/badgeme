import React, {useState, useEffect} from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink,
} from 'react-router-dom';

import {When} from 'react-if';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';

import Provider from './Provider';
import BadgeRequest from './BadgeRequest';
import TemplateCustomizer from './TemplateCustomizer';
import Designer from './Designer';
import Footer from './Footer';
import Gallery from './Gallery';
import Home from './Home';
import Map from './Map';
import NTAG21x from './NTAG21x';

import './App.css';

const activeClassName = 'active';

function App() {
	const [admin, setAdmin] = useState(false);

  useEffect(() => {
    async function checkAdmin() {
      try {
        const response = await fetch('/admin', {method: 'HEAD'});
        setAdmin(response.ok);
      } catch(e) {
        console.log(e);
      }
    }
    checkAdmin();
  }, []);

  return (
    <Router>
      <Navbar collapseOnSelect expand="sm" bg="light" variant="light">
        <Navbar.Brand as={NavLink} to="/" activeClassName={activeClassName}>
          Badges of
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link as={NavLink} to="/request" activeClassName={activeClassName}>
              Request
            </Nav.Link>
            <Nav.Link as={NavLink} to="/templates" activeClassName={activeClassName}>
              Templates
            </Nav.Link>
            <Nav.Link
              as={NavLink}
              to="/design"
              activeClassName={activeClassName}>
              Design
            </Nav.Link>
            <Nav.Link
              as={NavLink}
              to="/gallery"
              activeClassName={activeClassName}>
              Gallery
            </Nav.Link>
            <Nav.Link
              as={NavLink}
              to="/map"
              activeClassName={activeClassName}>
              Map
            </Nav.Link>
            <When condition={admin}>
              <Nav.Link className="text-danger bold" href="/admin">
                Admin
              </Nav.Link>
            </When>
            <Nav.Link href="https://gitlab.com/bettse/badgeme/">
              Sourcecode (GitLab)
            </Nav.Link>
          </Nav>
          <Nav>
            <Navbar.Text>
            </Navbar.Text>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      <Container fluid="md" className="mt-3">
        <Provider>
          <Switch>
            <Route path={["/request/:requestId", "/request"]} component={BadgeRequest} />
            <Route path={["/templates/:templateId", "/templates"]} component={TemplateCustomizer} />
            <Route path="/design" component={Designer} />
            <Route path="/gallery" component={Gallery} />
            <Route path="/uid/:uid" component={NTAG21x} />
            <Route path="/map" component={Map} />
            <Route path="/" component={Home} />
          </Switch>
        </Provider>
      </Container>
      <Footer />
    </Router>
  );
}

export default App;
