import React, {useState, useEffect} from 'react';
import {encodeToWeights} from 'gs1-128-encoder';

const DECIMAL = 10;

function Barcode128(props) {
  const {x, y, width, height, value} = props;
  const [encoded, setEncoded] = useState([]);
  const [valid, setValid] = useState(true);

  /*
   * TODO: more options:
   *  * with/without text
   *  * text below vs text above
   *  * spread text
   *  * justify text: center/left/right?
   */

  useEffect(() => {
    try {
      const encoded = encodeToWeights(value)
        .split('')
        .map(x => parseInt(x, DECIMAL));

      setEncoded(encoded);
      setValid(true);
    } catch (e) {
      setValid(false);
    }
  }, [value]);

  const barheight = height / 2;
  const sum = encoded.reduce((acc, x) => acc + x, 0);
  const unit = Math.max(width / sum, 1);

  function generateBarcode() {
    let x = 0;

    return encoded.map((weight, i) => {
      const width = weight * unit;
      const fill = i % 2 === 0 ? 'black' : 'white';
      x += width;
      return (
        <rect
          key={i}
          x={x - width}
          y="0"
          width={width}
          height={barheight}
          fill={fill}
        />
      );
    });
  }

  if (!valid) {
    return (
      <text
        textAnchor="middle"
        style={{fontSize: height, textTransform: 'uppercase'}}
        fill="red"
        x={x + width / 2}
        y={y + height}>
        Invalid barcode
      </text>
    );
  }

  return (
    <>
      <g transform={`translate(${x} ${y})`}>{generateBarcode()}</g>
      <text
        x={x + width / 2}
        y={y + height}
        textAnchor="middle"
        style={{fontSize: height / 2}}>
        {value}
      </text>
    </>
  );
}

export default Barcode128;
