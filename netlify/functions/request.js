const util = require("util");
const fs = require("fs");
const { createHash } = require("crypto");
const cookie = require('cookie')
const fetch = require("node-fetch");
const { InfluxDB, Point } = require("@influxdata/influxdb-client");
const Lob = require("lob");
const B2 = require("backblaze-b2");
const jsonwebtoken = require('jsonwebtoken');
const parser = require("lambda-multipart-parser");

const DB = require('./db');

const {
  NETLIFY_DEV = false,
  LOB_API_KEY,
  EMAIL_REP_KEY,
  JWT_SECRET,
  REQUEST_NOTIFICATION_URL
} = process.env;

const {
  INFLUXDB_V2_BUCKET,
  INFLUXDB_V2_TOKEN,
  INFLUXDB_V2_ORG,
  INFLUXDB_V2_URL
} = process.env;

const {
  BB_KEY_ID,
  BB_KEY_NAME,
  BB_APPLICATION_KEY,
  BB_BUCKET_ID,
} = process.env;

const approvedMime = ["image/jpeg", "image/png"];
const MAX_SIZE_BYTES = 1 * 1024 * 1024;
const EMAIL_REP = "https://emailrep.io";
const request_prefix = "requests";
const badge_prefix = "badges";
const gallery = "gallery";
const validVariants = [
  "primary",
  "secondary",
  "success",
  "danger",
  "warning",
  "info",
  "light",
  "dark"
];

const success = {
  statusCode: 200,
  body: ""
};

const failure = {
  statusCode: 401,
  body: JSON.stringify({
    status: "There was an error processing your request",
    statusVariant: "danger",
  })
};

const db = new DB();

const b2 = new B2({
  applicationKeyId: BB_KEY_ID,
  applicationKey: BB_APPLICATION_KEY
});

const writeApi = new InfluxDB({
  url: INFLUXDB_V2_URL,
  token: INFLUXDB_V2_TOKEN
}).getWriteApi(INFLUXDB_V2_ORG, INFLUXDB_V2_BUCKET, "ns", {
  writeFailed: console.log
});
writeApi.useDefaultTags({ dev: !!NETLIFY_DEV });

const { usVerifications } = Lob(LOB_API_KEY);
const verify = util.promisify(usVerifications.verify.bind(usVerifications));

async function addAddressValid(data) {
  const { name, address1, address2, city, region, postalcode, country } = data;
  // Can't verify non-us with this API
  if (['', 'USA', 'UNITED STATES'].includes(country.toUpperCase())) {
    try {
      const result = await verify({
        recipient: name,
        primary_line: address1,
        secondary_line: address2,
        city,
        state: region,
        zip_code: postalcode
      });
      const { deliverability, lob_confidence_score } = result;
      const { score, level } = lob_confidence_score;
      data['usVerifications'] = {deliverability, score, level}
    } catch (e) {
      console.log("address verification issue:", e);
    }
  }
}

//https://github.com/sublime-security/emailrep.io
async function addEmailReputation(data) {
  const { email } = data;
  try {
    const response = await fetch(`${EMAIL_REP}/${email}`, {
      headers: {
        Key: EMAIL_REP_KEY,
        "Content-Type": "application/json",
        "User-Agent": "badgesof.ericbetts.dev"
      }
    });
    const result = await response.json();
    if (response.ok) {
      const { reputation, suspicious } = result;
      data['emailReputation'] = {reputation, suspicious}
    } else {
      const { status, reason } = result;
      console.log("email rep error", status, result);
    }
  } catch (e) {
    console.log("error validating email", e);
  }
}

async function uploadToBB(badge, gallery = false) {
  const { content, contentType, sha1 } = badge;
  const parts = contentType.split("/");
  const ext = parts[parts.length - 1];

  try {
    await b2.authorize(); // must authorize first (authorization lasts 24 hrs)
    const { data } = await b2.getUploadUrl({ bucketId: BB_BUCKET_ID }); // returns promise
    const { uploadUrl, authorizationToken } = data;

    await b2.uploadFile({
      uploadUrl,
      uploadAuthToken: authorizationToken,
      fileName: `${badge_prefix}/${sha1}.${ext}`,
      mime: contentType,
      data: content
    });

    if (gallery) {
      const { filename } = badge;
      await b2.uploadFile({
        uploadUrl,
        uploadAuthToken: authorizationToken,
        fileName: `${gallery}/${filename}`,
        mime: contentType,
        data: content
      });
    }
  } catch (e) {
    console.log("upload error", e);
  }
}

async function notifyBackend(id) {
  if (NETLIFY_DEV) {
    return;
  }
  const response = await fetch(REQUEST_NOTIFICATION_URL, {
    method: "POST",
    body: JSON.stringify({ id })
  });
  console.log("POSTed to sockethook", response.ok);
}

async function updateStatus(id, status, statusVariant = "info") {
  if (!validVariants.includes(statusVariant)) {
    console.log("Invalid statusVariant", statusVariant);
    statusVariant = "primary";
  }
  const path = `badgesOf/request/${id}`;
  const url = `https://sockethook.ericbetts.dev/hook/${path}`;

  console.log("updateStatus", { id, status, statusVariant });
  try {
    const response = await fetch(url, {
      method: "POST",
      body: JSON.stringify({ status, statusVariant })
    });
  } catch (e) {
    console.log("updateStatus error", e);
  }
}

async function parseEvent(event) {
  try {
    const result = await parser.parse(event);
    const { files, ...rest } = result;
    if (!files || files.length < 1) {
      return failure;
    }
    const [file] = files;
    const { content, filename, contentType } = file;
    const size = content.length;

    const hash = createHash("sha1"); // B2 has a sha1 header, so I'm using that
    hash.update(content);
    const sha1 = hash.digest("hex");

    return {
      badge: {
        content,
        filename,
        contentType,
        size,
        sha1,
      },
      ...rest
    };
  } catch (e) {
    console.log(e);
    throw new Error("couldn't parse");
  }
}

async function requestBadge(event) {
  const { headers } = event;
  let point;
  const conn = await db.setup();
  const Request = conn.model('Request');

  try {
    const { badge, ...payload } = await parseEvent(event);
    const { country, email } = payload;
    const { content, contentType, size, filename, sha1 } = badge;

    point = new Point("request")
      .tag("country", country)
      .tag("type", contentType)
      .intField("size", size)
      .intField("value", 1);

    if (size > MAX_SIZE_BYTES) {
      point.tag("success", false);
      throw `Ignoring file of ${size /
        1024} kbytes over max of ${MAX_SIZE_BYTES}`;
    }

    if (!approvedMime.includes(contentType)) {
      point.tag("success", false);
      throw `Ignoring mime type ${contentType}`;
    }

    const isUser = await checkAuth(headers);
    // Avoid burning rate limit during development
    if (NETLIFY_DEV || isUser) {
      console.log("skip address validation");
    } else {
      await addAddressValid(payload);
      await addEmailReputation(payload);
    }

    const badgeMetadata = {contentType, size, filename, sha1}
    const request = new Request({
      ...payload,
      badge: badgeMetadata,
      status: 'persisted',
    });

    await request.save();
    const id = request._id;

    await uploadToBB(badge, payload.gallery);
    await updateStatus(id, "persisted");
    await notifyBackend(id);
    await updateStatus(id, "queued");

    point.tag("success", true);
    return {
      id,
      status: "queued",
      statusVariant: "info",
    }
  } catch (e) {
    console.error(e);
    point.tag("success", false);
    throw e;
  } finally {
    await writeApi.writePoint(point);
    //await writeApi.close();
    await writeApi.flush();
  }
}

async function checkAuth(headers) {
  try {
    if (!headers.cookie) {
      throw new Error('No Cookies');
    }

    const cookies = cookie.parse(headers.cookie);
    if (!cookies || !cookies.nf_jwt) {
      throw new Error('No JWT');
    }
    const { nf_jwt } = cookies;
    const decoded = await jsonwebtoken.verify(nf_jwt, JWT_SECRET);
    const { app_metadata } = decoded;
    const { authorization } = app_metadata;
    const { roles } = authorization
    if (!roles.includes('user')) {
      throw new Error('Not an admin');
    }
  } catch (e) {
    return false;
  }
  return true;
}

exports.handler = async function(event, context) {
  context.callbackWaitsForEmptyEventLoop = false;
  const { httpMethod, queryStringParameters, headers, body, isBase64Encoded } = event;

  const contentType = headers['content-type']

  try {
    const conn = await db.setup();
    const Request = conn.model('Request');

    if (httpMethod === 'GET') {
      const { id } = queryStringParameters;
      if (!id) {
        throw new Error('missing id');
      }

      const request = await Request.findById(id);
      if (!request) {
        throw new Error('no request found');
      }
      const { status = 'unknown' } = request;
      return {
        statusCode: 200,
        body: JSON.stringify({
          status,
          statusVariant: 'secondary',
        }),
      };

    } else if (httpMethod === 'POST') {
      const body = await requestBadge(event);
      return {
        statusCode: 200,
        body: JSON.stringify(body),
      };
    }
  } catch (e) {
    console.error(e);
    return {
      statusCode: 422,
      body: JSON.stringify({
        status: e,
        statusVariant: "danger"
      })
    };
  }
  return failure;
};
