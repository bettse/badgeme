const {getCode} = require('country-list');
const { InfluxDB, Point } = require("@influxdata/influxdb-client");

const {
  INFLUXDB_V2_BUCKET,
  INFLUXDB_V2_TOKEN,
  INFLUXDB_V2_ORG,
  INFLUXDB_V2_URL
} = process.env;

const failure = {
  statusCode: 503,
  body: JSON.stringify({}),
};

const queryApi = new InfluxDB({
  url: INFLUXDB_V2_URL,
  token: INFLUXDB_V2_TOKEN
}).getQueryApi(INFLUXDB_V2_ORG);

const query = `
from(bucket: "${INFLUXDB_V2_BUCKET}")
  |> range(start: -1y)
  |> filter(fn: (r) => r["_measurement"] == "request")
  |> filter(fn: (r) => r["_field"] == "value")
  |> filter(fn: (r) => r["success"] == true)
  |> aggregateWindow(every: inf, fn: count)
  |> group(columns: ["country"])
  |> sum(column: "_value")
  |> group()
`;


// country-list doesn't match some
const countryRename = {
  'Norge': 'Norway',
  USA: 'United States of America',
  '': 'United States of America',
  'United States': 'United States of America',
  'United Kingdom': 'United Kingdom of Great Britain and Northern Ireland',
};

exports.handler = async function(event, context) {
  try {

		const results = await queryApi.collectRows(query);
    const countries = {};

    results.forEach(row => {
      const {country, _value: count} = row;
      const code = getCode(countryRename[country] || country);
      if (!code) {
        console.log("No code for", country);
        return;
      }
      if (!countries[code]) {
        countries[code] = 0;
      }
      countries[code] = countries[code] + count;
    });

    return {
      statusCode: 200,
      body: JSON.stringify({
        countries,
      }),
    };
  } catch (e) {
    console.log(e);
  }
  return failure;
};
