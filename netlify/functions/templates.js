const parser = require('lambda-multipart-parser');
const B2 = require('backblaze-b2');

const {
  BB_KEY_ID, BB_KEY_NAME, BB_APPLICATION_KEY,
  BB_BUCKET_ID, BB_BUCKET_NAME,
  REQUEST_NOTIFICATION_URL,
} = process.env;

const templates_prefix = 'templates'

const failure = {
  statusCode: 503,
  body: JSON.stringify({}),
};

const b2 = new B2({
  applicationKeyId: BB_KEY_ID,
  applicationKey: BB_APPLICATION_KEY,
});

async function getTemplates() {
  try {
    const { data: auth } = await b2.authorize();
    const { downloadUrl } = auth;

    const { data } = await b2.listFileNames({
      bucketId: BB_BUCKET_ID,
      maxFileCount: 100,
      prefix: templates_prefix,
    });
    const { files } = data;

    const { data: downloadAuth } = await b2.getDownloadAuthorization({
      bucketId: BB_BUCKET_ID,
      fileNamePrefix: templates_prefix,
      validDurationInSeconds: 24 * 60 * 60,
    });
    const { authorizationToken } = downloadAuth;

    return files.map(file => {
      const { fileName } = file;
      const parts = fileName.split('/')
      const name = parts[parts.length - 1];
      const url = `${downloadUrl}/file/${BB_BUCKET_NAME}/${fileName}?Authorization=${authorizationToken}`
      return {name, url};
    });
  } catch (e) {
    console.log(e);
    return [];
  }
  return [];
}

async function submitTemplate(event) {
  try {
    const result = await parser.parse(event);
    const { files, name, secret } = result;
    const [file] = files;
    const { content, filename, contentType } = file;
    console.log(file, 'size', content.length);

    await b2.authorize(); // must authorize first (authorization lasts 24 hrs)
    const { data } = await b2.getUploadUrl({bucketId: BB_BUCKET_ID});  // returns promise
    const { uploadUrl, authorizationToken } = data;

    await b2.uploadFile({
      uploadUrl,
      uploadAuthToken: authorizationToken,
      fileName: `${templates_prefix}/${filename}`,
      mime: contentType,
      data: content,
      info: {
        name, secret,
      },
    });

    // TODO: save secret and name and support deleting
  } catch (e) {
    console.log('upload error', e)
  }
}

exports.handler = async function(event, context) {
  const { httpMethod, headers } = event;
  if (httpMethod === 'GET') {
    const templates = await getTemplates();
    return {
      statusCode: 200,
      body: JSON.stringify({
        templates,
      }),
    };
  } else if (httpMethod === 'POST') {
    const contentType = headers['content-type'];
    if (contentType.startsWith('multipart/form-data')) {
      await submitTemplate(event);
      return {
        statusCode: 204,
      }
    } // support json later?
  }
  return failure;
};
