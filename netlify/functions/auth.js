const cookie = require("cookie");
const { sign } = require("jsonwebtoken");

const { JWT_SECRET, NETLIFY_DEV } = process.env;

const hour = 3600000;
const twoWeeks = 14 * 24 * hour;
const ttl = 24 * 60 * 60;

const failure = {
  statusCode: 401,
  body: JSON.stringify({})
};

const error = {
  statusCode: 503,
  body: JSON.stringify({})
};

exports.handler = event => {
  if (!NETLIFY_DEV) {
    return failure;
  }

  const userData = {
    id: "test",
    exp: Math.floor(Date.now() / 1000) + ttl,
    app_metadata: {
      authorization: {
        roles: ["user", "admin"]
      }
    }
  };

  const jwt = sign(userData, "secret");

  const myCookie = cookie.serialize("nf_jwt", jwt, {
    secure: !NETLIFY_DEV,
    httpOnly: true,
    path: "/",
    maxAge: twoWeeks
  });

  return {
    statusCode: 200,
    headers: {
      "Set-Cookie": myCookie,
      "Cache-Control": "no-cache"
    }
  };
  return failure;
};
