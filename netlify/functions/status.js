const cookie = require('cookie')
const { verify } = require('jsonwebtoken');

const DB = require('./db');

const { NODE_ENV, JWT_SECRET } = process.env;

const failure = {
  statusCode: 503,
  body: JSON.stringify({}),
};

const db = new DB();

async function checkAuth(headers) {
  if (!headers.cookie) {
    throw new Error('No Cookies');
  }

  const cookies = cookie.parse(headers.cookie);
  if (!cookies || !cookies.nf_jwt) {
    throw new Error('No JWT');
  }
  const { nf_jwt } = cookies;
  const decoded = await verify(nf_jwt, JWT_SECRET);
  const { app_metadata } = decoded;
  const { authorization } = app_metadata;
  const { roles } = authorization
  if (!roles.includes('admin')) {
    throw new Error('Not an admin');
  }
}

exports.handler = async function(event, context) {
  context.callbackWaitsForEmptyEventLoop = false;
  const { httpMethod, headers } = event;

  try {
    const conn = await db.setup();
    const PrinterStatus = conn.model('PrinterStatus');

    if (httpMethod === 'GET') {
      const status = await PrinterStatus.findOne({name: "EvolisPrimacy"});
      return {
        statusCode: 200,
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          status,
        }),
      };
    } else if (httpMethod === 'POST') {
      await checkAuth(headers);
      const {body: json} = event;
      const body = JSON.parse(json);
      body['last_updated'] = new Date();
      const update = {
        stock: body,
      }

      await PrinterStatus.findOneAndUpdate({name: "EvolisPrimacy"}, update);
      return {
        statusCode: 204,
      };
    }
  } catch (e) {
    return {
      statusCode: 401,
      body: JSON.stringify({ msg: e.message }),
    };
  } finally {
    if (NODE_ENV === 'development') {
      await db.close();
    }
  }
}
