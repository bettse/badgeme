const cookie = require('cookie')
const B2 = require('backblaze-b2');
const { verify } = require('jsonwebtoken');
const fetch = require('node-fetch');

const DB = require('./db');

const {
  BB_KEY_ID, BB_KEY_NAME, BB_APPLICATION_KEY,
  BB_BUCKET_ID, BB_BUCKET_NAME,
} = process.env;

const {
  NODE_ENV, REQUEST_NOTIFICATION_URL, JWT_SECRET,
} = process.env;

const failure = {
  statusCode: 503,
  body: JSON.stringify({}),
};

const request_prefix = 'requests';
const badge_prefix = 'badges';
const db = new DB();

const b2 = new B2({
  applicationKeyId: BB_KEY_ID,
  applicationKey: BB_APPLICATION_KEY,
});

async function notifyBackend(id, printCard = true, printLetter = true) {
  const response = await fetch(REQUEST_NOTIFICATION_URL, {
    method: 'POST',
    body: JSON.stringify({id, printCard, printLetter}),
  });
  console.log('POSTed to sockethook', response.ok);
}

async function getRequests() {
  try {
    const conn = await db.setup();
    const Request = conn.model('Request');

    const { data: auth } = await b2.authorize();
    const { downloadUrl } = auth;

    const { data: downloadAuth } = await b2.getDownloadAuthorization({
      bucketId: BB_BUCKET_ID,
      validDurationInSeconds: 24 * 60 * 60,
      fileNamePrefix: badge_prefix,
    });
    const { authorizationToken } = downloadAuth;

    const requests = await Request.find().sort({updatedAt: 'desc'}).limit(10);
    return requests.map(r => {
      const request = r.toObject();
      const { badge } = request;
      const { contentType, sha1 } = badge;
      const parts = contentType.split("/");
      const ext = parts[parts.length - 1];

      const url = `${downloadUrl}/file/${BB_BUCKET_NAME}/${badge_prefix}/${sha1}.${ext}?Authorization=${authorizationToken}`
      return {
        ...request,
        badge: {url, ...badge},
      }
    });
  } catch (e) {
    console.log(e);
    return [];
  } finally {
    if (NODE_ENV === 'development') {
      await db.close();
    }
  }

  return [];
}

async function checkAuth(headers) {
  if (!headers.cookie) {
    throw new Error('No Cookies');
  }

  const cookies = cookie.parse(headers.cookie);
  if (!cookies || !cookies.nf_jwt) {
    throw new Error('No JWT');
  }
  const { nf_jwt } = cookies;
  const decoded = await verify(nf_jwt, JWT_SECRET);
  const { app_metadata } = decoded;
  const { authorization } = app_metadata;
  const { roles } = authorization
  if (!roles.includes('admin')) {
    throw new Error('Not an admin');
  }
}

async function deleteRequest(id) {
  try {
    const conn = await db.setup();
    const Request = conn.model('Request');
    await Request.findByIdAndDelete(id);
  } catch (e) {
    console.log('failure in deleting request', e);
  }
}

exports.handler = async function(event, context) {
  context.callbackWaitsForEmptyEventLoop = false;
  const { httpMethod, headers } = event;
  try {
    await checkAuth(headers);
  } catch (e) {
    return {
      statusCode: 401,
      body: httpMethod === 'HEAD' ? undefined : JSON.stringify({ msg: e.message }),
    };
  }

  if (httpMethod === 'HEAD') {
    return {
      statusCode: 200,
    };
  } else if (httpMethod === 'GET') {
    const requests = await getRequests();
    return {
      statusCode: 200,
      body: JSON.stringify({
        requests,
      }),
    };
  } else if (httpMethod === 'POST') {
    const {body: json} = event;
    try {
      const body = JSON.parse(json);
      const {id, card = true, letter = true} = body;
      if (!id) {
        return failure;
      }

      await notifyBackend(id, card, letter);
    } catch (e) {
      return {
        statusCode: 401,
        body: JSON.stringify({ msg: e.message }),
      };
    }

    return {
      statusCode: 204,
    };
  } else if (httpMethod === 'DELETE') {
    const {body: json} = event;
    try {
      const body = JSON.parse(json);
      const {id} = body;
      if (!id) {
        throw new Error('missing id');
      }

      await deleteRequest(id);
    } catch (e) {
      return {
        statusCode: 401,
        body: JSON.stringify({ msg: e.message }),
      };
    }
    return {
      statusCode: 204,
    };
  }
  return failure;
};
