require("dotenv").config();
const mongoose = require("mongoose");
const { Schema } = mongoose;

const { MONGODB_CONNECTION_STRING } = process.env;

process.on('SIGINT', () => {
  mongoose.connection.close(() => {
    console.log('Mongoose default connection disconnected through app termination');
    process.exit(0);
  });
});

const Request = new Schema(
  {
    uuid: { type: String, index: true },
    name: String,
    address1: String,
    address2: String,
    city: String,
    region: String,
    postalcode: String,
    country: String,
    email: String,
    notes: String,
    uid: { type: String, index: true },
    status: String,
    usVerifications: {
      deliverability: String,
      score: Number,
      level: String,
    },
    emailReputation: { reputation: String, suspicious: Boolean },
    url: String,
    // TODO: support front and back
    badge: {
      contentType: String,
      filename: String,
      size: Number,
      sha1: String,
    },
  },
  { timestamps: true }
);

const PrinterStatus = new Schema(
  {
    name: { type: String, index: true },
    ribbon: String, //Rr
    // RFID Ribbon tag
    tag: {
      // Rrt;<keyword>
      zone: String, // Ribbon zone
      label: String, // Ribbon label
      ref: String, // Product code
      date: String, // Manufactured date
      qty: Number, // Ribbon capacity
      count: Number, //Number of printing available
    },
    status: {
      // Rlr;<character>
      ribbon_finished: String,
      card_position: String,
      feeder: String,
      cleaning: String,
      hopper: String,
      bezel: String,
    },
    stock: {
      card_type: String,
      global_stamps: Number,
      us_stamps: Number,
      envelopes: Number,
      last_updated: Date,
    },
  },
  { timestamps: true }
);

class DB {
  constructor() {
    this.conn = null;
  }

  async setup() {
    if (this.conn == null) {
      this.conn = mongoose.createConnection(MONGODB_CONNECTION_STRING, {
        serverSelectionTimeoutMS: 3000,
      });

      await this.conn;

      this.conn.model("Request", Request);
      this.conn.model("PrinterStatus", PrinterStatus);
    }
    return this.conn;
  }

  async close() {
    await this.conn.close()
  }
}

module.exports = DB;
