const B2 = require('backblaze-b2');

const {
  BB_KEY_ID, BB_KEY_NAME, BB_APPLICATION_KEY,
  BB_BUCKET_ID, BB_BUCKET_NAME,
  REQUEST_NOTIFICATION_URL,
} = process.env;

const prefix = 'gallery'

const failure = {
  statusCode: 503,
  body: JSON.stringify({}),
};

const b2 = new B2({
  applicationKeyId: BB_KEY_ID,
  applicationKey: BB_APPLICATION_KEY,
});

async function getBadges() {
  try {
    const { data: auth } = await b2.authorize();
    const { downloadUrl } = auth;

    const { data } = await b2.listFileNames({
      bucketId: BB_BUCKET_ID,
      maxFileCount: 100,
      prefix,
    });
    const { files } = data;

    const { data: downloadAuth } = await b2.getDownloadAuthorization({
      bucketId: BB_BUCKET_ID,
      fileNamePrefix: prefix,
      validDurationInSeconds: 24 * 60 * 60,
    });
    const { authorizationToken } = downloadAuth;

    return files.map(file => {
      const { fileName, contentType: type, contentLength: size } = file;
      const parts = fileName.split('/')
      const filename = parts[parts.length - 1];
      const url = `${downloadUrl}/file/${BB_BUCKET_NAME}/${fileName}?Authorization=${authorizationToken}`
      return {filename, type, size, url};
    });
  } catch (e) {
    console.log(e);
    return [];
  }
  return [];
}

exports.handler = async function(event, context) {
  const { httpMethod } = event;
  if (httpMethod === 'GET') {
    const badges = await getBadges();
    return {
      statusCode: 200,
      body: JSON.stringify({
        badges,
      }),
    };
  }
  return failure;
};
